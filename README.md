# rain-world-database-api-litedb

<img src="https://gitlab.com/Rawra/rain-world-database-api-litedb/-/raw/main/docs/logo/logo.png" alt="showcase-1" width="600"/>

<img src="https://gitlab.com/Rawra/rain-world-database-api-litedb/-/raw/main/docs/thumbnail.png" alt="showcase-1" width="600"/>

## What it is

This project makes it easier to store Save or Creature related data in a persistant way
without interacing with the games actual save file system.

## How it works

To achieve this we use the [LiteDB](https://github.com/mbdavid/LiteDB) NoSQL database.
Its quite performant as it uses an internal cache, making write/read ops significantly faster than SQLite for comparison. The tradeoff being we don't have the real power of a relational database at our hands, but a rather more neat and tidy document bson-based database.

## Features

- Make custom data tables associated with specific game save-states
- Make custom data tables associated with specific creatures (depending on save file and id)

## Examples

You can add, for instance, a creature like so:
```cs
// Get the current save
Option<SaveEntry> save = RWDB.LiteDB.Core.SaveDataManager.Instance.SavesRepository.GetByChecksum(instanceId);
if (!save.HasValue)
{
    Globals.mls.LogInfo($"TryAddTamedLizard: GetByChecksum returned null");
    return false;
}

// Add the creature
CreatureEntry creatureEntry = new CreatureEntry(creature);
Option<CreatureEntry> addedCreature = RWDB.LiteDB.Core.SaveDataManager.Instance.CreatureRepository.AddIfNotExists(save.ValueOrFailure(), creatureEntry);
if (!addedCreature.HasValue)
{
    Globals.mls.LogInfo($"TryAddTamedLizard: CreatureRepository.AddIfNotExists -- returned false");
    return false;
}
Globals.mls.LogInfo($"TryAddTamedLizard: CreatureRepository.AddIfNotExists, added id={creatureEntry.Id}");
```

The full API is implemented in CRUD-style.
For a full reference to the available API, see: 

- ISaveRepository
- SaveRepositoryLiteDB
- ICreatureRepository
- CreatureRepositoryLiteDB

The approach here was Try- and Optional\<T\> oriented for quality of life:
```
public interface ISavesRepository<T>
{
    long Add(T value);
    Option<T> AddIfNotExists(T value);
    DBRepositoryResult TryAddOrUpdate(T value);
    bool TryUpdate(T value);
    DBRepositoryResult TryRemove(T value);
    IEnumerable<T> GetAll();
    Option<T> GetById(long id);
    Option<T> GetByChecksum(uint checksum); // checks = Adler32 of Slugcat Name
}
```

## Building

If you're not compiling from scratch, it should suffice to clone this repository and just building the sln.
You may want to change gameinstalldir.txt before compiling for quality of life.

## Credits

| Name | Link
|-|-
| LiteDB | https://github.com/mbdavid/LiteDB