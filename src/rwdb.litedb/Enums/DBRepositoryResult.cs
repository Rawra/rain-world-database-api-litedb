﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWDB.LiteDB.Enums
{
    [Flags]
    public enum DBRepositoryResult
    {
        Error = 0,
        Success = 1,
        Created = 2,
        Read = 4,
        Updated = 8,
        Deleted = 16
    };
}
