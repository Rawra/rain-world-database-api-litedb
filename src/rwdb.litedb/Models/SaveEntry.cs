﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LiteDB;

namespace RWDB.LiteDB.Models
{
    public class SaveEntry
    {
        [BsonId(true)]
        public int Id { get; set; }
        public uint Checksum { get; set; }
        public string Slugcat { get; set; }

        [BsonRef("CreatureEntry")]
        public List<CreatureEntry> Creatures { get; set; }

        public SaveEntry()
        {
            Creatures = new List<CreatureEntry>();
        }
    }
}
