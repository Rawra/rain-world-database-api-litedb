﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWDB.LiteDB.Models
{
    public class CreatureEntry
    {
        [BsonId(true)]
        public long Id { get; set; }
        public int GameId { get; set; }

        public CreatureEntry(AbstractCreature creature)
        {
            this.GameId = creature.ID.number;
        }
        public CreatureEntry() { }
    }
}
