﻿using RWDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using On;
using RWCustom;
using MoreSlugcats;
using RWDB.LiteDB.Core;
using Expedition;

namespace RWDB.LiteDB.Hooks
{
    internal class HookRainWorldGame
    {
        /// <summary>
        /// Rain World Constructor hook: To make sure to keep the WorldInstance variable
        /// up-to-date with the current in-game mode
        /// BEGIN
        /// </summary>
        /// <param name="orig"></param>
        /// <param name="self"></param>
        /// <param name="manager"></param>
        public static void RainWorldGame_ctor1(On.RainWorldGame.orig_ctor orig, RainWorldGame self, ProcessManager manager)
        {
            orig.Invoke(self, manager);

            var character = Custom.rainWorld.progression.miscProgressionData.currentlySelectedSinglePlayerSlugcat;
            Globals.mls.LogInfo($"[RainWorldGame_ctor1]: character: {character.ToString()}");

            SaveDataManager.Instance.BeginTransaction(character.ToString(), self.rainWorld.ExpeditionMode);
        }
    }
}
