﻿using RWCustom;
using RWDB.LiteDB.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWDB.LiteDB.Hooks
{
    internal class HooksSaveState
    {

        public static void SaveState_SessionEnded(On.SaveState.orig_SessionEnded orig, SaveState self, RainWorldGame game, bool survived, bool newMalnourished)
        {
            orig.Invoke(self, game, survived, newMalnourished);

            string character = self.saveStateNumber.ToString();
            Globals.mls.LogInfo($"[SaveState_SessionEnded] character={character}, expedition={Custom.rainWorld.ExpeditionMode}, survived={survived}");

            if (!survived)
            {
                SaveDataManager.Instance.Rollback(character, Custom.rainWorld.ExpeditionMode);
            } 
            else
            {
                SaveDataManager.Instance.CommitTransaction(character, Custom.rainWorld.ExpeditionMode);
            }
        }
    }
}
