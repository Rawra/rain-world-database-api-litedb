﻿using Optional;
using Optional.Unsafe;
using RWDB.LiteDB.Core;
using RWDB.LiteDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWDB.LiteDB.Hooks
{
    internal class HooksAbstractCreature
    {
        // ToDo: Add simple and performant "IsDatabaseCreature" or "IsCreatureTracked" via dictionary(?) to see
        // if we should even do this check here, and it can additionally speed up similiar comparisons elsewhere and in the future.
        // So we can avoid doing any kind of database lookup if we dont need to in the first place.
        public static void AbstractCreature_Die(On.AbstractCreature.orig_Die orig, AbstractCreature self)
        {
            orig.Invoke(self);

            Option<SaveEntry> save = SaveDataManager.Instance.SavesRepository.GetByChecksum(SaveDataManager.Instance.WorldInstance);
            if (!save.HasValue)
            {
                //Globals.mls.LogInfo($"AbstractCreature_Die: GetByChecksumAsync: null");
                return;
            }

            Option<CreatureEntry> creature = SaveDataManager.Instance.CreatureRepository.GetByGameId(save.ValueOrFailure(), self.ID.number);
            if (!creature.HasValue)
            {
                Globals.mls.LogInfo($"AbstractCreature_Die: GetByGameIdAsync: null");
                return;
            }

            if (SaveDataManager.Instance.CreatureRepository.TryRemove(save.ValueOrFailure(), creature.ValueOrFailure()) != Enums.DBRepositoryResult.Success)
            {
                Globals.mls.LogInfo($"AbstractCreature_Die: TryRemoveAsync: false");
                return;
            }
        }
    }
}
