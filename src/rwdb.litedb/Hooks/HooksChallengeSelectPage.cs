﻿using Expedition;
using RWCustom;
using RWDB.LiteDB.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWDB.LiteDB.Hooks
{
    internal class HooksChallengeSelectPage
    {
        public static void ChallengeSelectPage_StartGame(On.Menu.ChallengeSelectPage.orig_StartGame orig, Menu.ChallengeSelectPage self)
        {
            orig.Invoke(self);

            var character = ExpeditionData.slugcatPlayer.ToString();
            Globals.mls.LogInfo($"[ChallengeSelectPage_StartGame]: character: {character}, expedition={Custom.rainWorld.ExpeditionMode}");

            SaveDataManager.Instance.TryAddSave(character.ToString(), true);
        }
    }
}
