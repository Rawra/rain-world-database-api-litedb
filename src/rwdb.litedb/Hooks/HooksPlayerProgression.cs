﻿using RWCustom;
using RWDB.LiteDB.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWDB.LiteDB.Hooks
{
    internal class HooksPlayerProgression
    {
        public static void PlayerProgression_WipeSaveState(On.PlayerProgression.orig_WipeSaveState orig, PlayerProgression self, SlugcatStats.Name saveStateNumber)
        {
            orig.Invoke(self, saveStateNumber);

            string character = saveStateNumber.ToString();
            Globals.mls.LogInfo($"[PlayerProgression_WipeSaveState] character={character}");

            SaveDataManager.Instance.TryDeleteSave(character, Custom.rainWorld.ExpeditionMode);
        }
    }
}
