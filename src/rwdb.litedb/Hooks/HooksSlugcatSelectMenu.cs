﻿using RWDB.LiteDB.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWDB.LiteDB.Hooks
{
    internal class HooksSlugcatSelectMenu
    {
        public static void SlugcatSelectMenu_StartGame(On.Menu.SlugcatSelectMenu.orig_StartGame orig, Menu.SlugcatSelectMenu self, SlugcatStats.Name storyGameCharacter)
        {
            orig.Invoke(self, storyGameCharacter);

            var character = storyGameCharacter.ToString();
            Globals.mls.LogInfo($"[SlugcatSelectMenu_StartGame]: character: {character}");

            SaveDataManager.Instance.TryAddSave(character.ToString(), true);
        }
    }
}
