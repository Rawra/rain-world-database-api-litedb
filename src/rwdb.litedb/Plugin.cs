﻿using BepInEx;
using HarmonyLib;
using RWCustom;
using RWDB.LiteDB.Core;
using RWDB.LiteDB.Hooks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace RWDB
{
    [BepInPlugin(PLUGIN_GUID, PLUGIN_NAME, PLUGIN_VERSION)]
    public class Plugin : BaseUnityPlugin
    {
        public const string PLUGIN_GUID = "rwdb.litedb";
        public const string PLUGIN_NAME = "Rain World Database API: LiteDB";
        public const string PLUGIN_VERSION = "1.0.0";

        private void Awake()
        {
            try
            {
                Globals.mls = Logger;
                Logger.LogInfo($"Plugin {PLUGIN_NAME} is loading!");

                // Harmony Patches Initialization
                var harmony = new Harmony("com.rwdblitedb.patch");
                harmony.PatchAll();

                // On.Hooks Initialization
                On.RainWorld.OnModsInit += RainWorld_OnModsInit;

                if (!DapperManager.Instance.TrySessionSetup())
                {
                    Logger.LogInfo($"Session setup completed with errors!");
                    return;
                }

                // Plugin startup logic
                Logger.LogInfo($"Plugin {PLUGIN_NAME} is loaded!");
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
            }
        }

        /// <summary>
        /// Callback for mod initialization (hook setup stage)
        /// </summary>
        /// <param name="orig"></param>
        /// <param name="self"></param>
        private void RainWorld_OnModsInit(On.RainWorld.orig_OnModsInit orig, RainWorld self)
        {
            try
            {
                Logger.LogInfo("RainWorld_OnModsInit: Settings up hooks");
                On.RainWorldGame.ctor += HookRainWorldGame.RainWorldGame_ctor1;
                On.SaveState.SessionEnded += HooksSaveState.SaveState_SessionEnded;
                On.PlayerProgression.WipeSaveState += HooksPlayerProgression.PlayerProgression_WipeSaveState;
                On.Menu.SlugcatSelectMenu.StartGame += HooksSlugcatSelectMenu.SlugcatSelectMenu_StartGame;
                On.Menu.ChallengeSelectPage.StartGame += HooksChallengeSelectPage.ChallengeSelectPage_StartGame;
                On.AbstractCreature.Die += HooksAbstractCreature.AbstractCreature_Die;

            }
            catch (Exception ex)
            {
                Logger.LogError("RainWorld_OnModsInit Error: " + ex.ToString());
            }
            finally
            {
                orig.Invoke(self);
            }
        }
    }
}
