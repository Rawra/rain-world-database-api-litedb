﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using LiteDB;
using RWDB.LiteDB.Models;
namespace RWDB.LiteDB.Core
{
    public class DapperManager
    {
        private static readonly Lazy<DapperManager> lazy = new Lazy<DapperManager>(() => new DapperManager());
        public static DapperManager Instance
        {
            get
            {
                return lazy.Value;
            }
        }

        private DapperManager()
        {

        }

        public LiteDatabase LiteDatabase { get; private set; }

        /// <summary>
        /// LiteDB in-memory collections
        /// </summary>
        public ILiteCollection<SaveEntry> Saves { get; private set; }
        public ILiteCollection<CreatureEntry> Creatures { get; private set; }

        public bool TrySessionSetup()
        {
            Globals.mls.LogInfo("TrySessionSetup()");
            string filePath = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "../data/rw-database-api-litedb.db");
            LiteDatabase = new LiteDatabase(filePath);

            if (!TrySetupTables()) { 
                Globals.mls.LogInfo("TrySessionSetup() - Error while creating tables!");
                return false;
            }

            Globals.mls.LogInfo("TrySessionSetup() - OK");
            return true;
        }

        /// <summary>
        /// Creates all the tables for an "initial" state of db
        /// </summary>
        private bool TrySetupTables()
        {
            Globals.mls.LogInfo("TrySetupTables()");

            Saves = LiteDatabase.GetCollection<SaveEntry>();
            Creatures = LiteDatabase.GetCollection<CreatureEntry>();

            // Setup collection indexes
            Saves.EnsureIndex(x => x.Creatures);
            //Creatures.EnsureIndex(x => x.GameId);

            Globals.mls.LogInfo("TrySetupTables() - OK");
            return true;
        }

    }
}
