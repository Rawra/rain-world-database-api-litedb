﻿using Optional;
using Optional.Unsafe;
using RWDB.LiteDB.Enums;
using RWDB.LiteDB.Models;
using RWDB.LiteDB.Repositories;
using System;
using System.Text;

namespace RWDB.LiteDB.Core
{
    public class SaveDataManager
    {
        private static readonly Lazy<SaveDataManager> lazy = new Lazy<SaveDataManager>(() => new SaveDataManager());
        public static SaveDataManager Instance
        {
            get
            {
                return lazy.Value;
            }
        }

        private SaveDataManager()
        {
            savesRepository = new SaveRepositoryLiteDB();
            creatureRepository = new CreatureRepositoryLiteDB();
        }

        /// <summary>
        /// The current world-instance (differs depending on save/slug cat: ie campaign or expedition)
        /// is used to identify specific saves from each other when using the sqlite db
        /// </summary>
        private uint worldInstance;
        public uint WorldInstance { 
            get { return worldInstance; }
            set {
                worldInstance = value;
                Globals.mls.LogInfo($"[SetWorldInstance]: instanceID: {worldInstance}");
            } 
        }

        /// <summary>
        /// CRUD Repositories
        /// </summary>
        private SaveRepositoryLiteDB savesRepository;
        public SaveRepositoryLiteDB SavesRepository
        {
            get { return savesRepository; }
            private set { savesRepository = value; }
        }

        private CreatureRepositoryLiteDB creatureRepository;
        public CreatureRepositoryLiteDB CreatureRepository
        {
            get { return creatureRepository; }
            private set { creatureRepository = value; }
        }

        /// <summary>
        /// Get checksum by slugcat name and expedition mode boolean
        /// </summary>
        /// <param name="slugcatName"></param>
        /// <param name="expedition"></param>
        /// <returns></returns>
        public uint GetChecksumBySlugcat(string slugcatName, bool expedition)
        {
            byte[] slugcatChecksumBytes;

            if (expedition)
                slugcatChecksumBytes = UTF8Encoding.UTF8.GetBytes(slugcatName + "-expedition");
            else
                slugcatChecksumBytes = UTF8Encoding.UTF8.GetBytes(slugcatName);

            uint instanceId = 1;
            instanceId = Ionic.Zlib.Adler.Adler32(instanceId, slugcatChecksumBytes, 0, slugcatChecksumBytes.Length);
            Globals.mls.LogInfo($"[GetChecksumBySlugcat]: slugcatName: {slugcatName}, expedition={expedition}, instanceId={instanceId}");
            return instanceId;
        }

        /// <summary>
        /// Add a save to the database
        /// </summary>
        /// <param name="slugcatName"></param>
        /// <param name="expedition"></param>
        /// <returns></returns>
        public bool TryAddSave(string slugcatName, bool expedition = false)
        {
            Globals.mls.LogInfo($"TryAddSave, slugcatName={slugcatName}, expedition={expedition}");
            uint instanceId = GetChecksumBySlugcat(slugcatName, expedition);

            SaveEntry save = new SaveEntry()
            {
                Checksum = instanceId,
                Slugcat = slugcatName
            };
            if (SavesRepository.TryAddOrUpdate(save) != DBRepositoryResult.Success)
            {
                Globals.mls.LogInfo($"TryAddSave, slugcatName={slugcatName}, expedition={expedition} -- SavesRepository.TryAddOrUpdate returned error");
                return false;
            }

            Globals.mls.LogInfo($"TryAddSave, slugcatName={slugcatName}, expedition={expedition} - Added save id: {save.Id}");
            return true;
        }

        /// <summary>
        /// Delete a save from the database
        /// </summary>
        /// <param name="slugcatName"></param>
        /// <param name="expedition"></param>
        /// <returns></returns>
        public bool TryDeleteSave(string slugcatName, bool expedition = false)
        {
            Globals.mls.LogInfo($"TryDeleteSave, slugcatName={slugcatName}, expedition={expedition}");
            uint instanceId = GetChecksumBySlugcat(slugcatName, expedition);

            Option<SaveEntry> save = SavesRepository.GetByChecksum(instanceId);
            if (!save.HasValue)
            {
                Globals.mls.LogInfo($"TryDeleteSave, slugcatName={slugcatName}, expedition={expedition} -- GetByChecksum: returned null");
                return false;
            }
            SavesRepository.TryRemove(save.ValueOrFailure());

            return true;
        }

        /// <summary>
        /// Begin world transaction
        /// </summary>
        /// <param name="slugcatName"></param>
        /// <param name="expedition"></param>
        public bool BeginTransaction(string slugcatName, bool expedition = false)
        {
            Globals.mls.LogInfo($"BeginTransaction, slugcatName={slugcatName}, expedition={expedition}");
            uint instanceId = GetChecksumBySlugcat(slugcatName, expedition);
            WorldInstance = instanceId;

            DapperManager.Instance.LiteDatabase.BeginTrans();
            return true;
        }

        /// <summary>
        /// Commit world transaction
        /// </summary>
        /// <param name="slugcatName"></param>
        /// <param name="expedition"></param>
        public bool CommitTransaction(string slugcatName, bool expedition = false)
        {
            Globals.mls.LogInfo($"CommitTransaction, slgucatName={slugcatName}, expedition={expedition}");
            uint instanceId = GetChecksumBySlugcat(slugcatName, expedition);
            WorldInstance = instanceId;

            return DapperManager.Instance.LiteDatabase.Commit();
        }


        /// <summary>
        /// Rollback world transaction
        /// </summary>
        /// <param name="slugcatName"></param>
        /// <param name="expedition"></param>
        public bool Rollback(string slugcatName, bool expedition = false)
        {
            Globals.mls.LogInfo($"Rollback, slugcatName={slugcatName}, expedition={expedition}");
            uint instanceId = GetChecksumBySlugcat(slugcatName, expedition);
            WorldInstance = instanceId;

            return DapperManager.Instance.LiteDatabase.Rollback();
        }
    }
}
