﻿using RWDB.LiteDB.Models;
using RWDB.LiteDB.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optional;
using RWDB.LiteDB.Enums;

namespace RWDB.LiteDB.Repositories
{
    public class SaveRepositoryLiteDB : ISavesRepository<SaveEntry>
    {
        // == Add/Update/Remove ==

        public long Add(SaveEntry value)
        {
            return DapperManager.Instance.Saves.Insert(value);
        }

        public Option<SaveEntry> AddIfNotExists(SaveEntry value)
        {
            if (DapperManager.Instance.Saves.Exists(x => x.Checksum == value.Checksum))
                return Option.None<SaveEntry>();

            DapperManager.Instance.Saves.Insert(value);
            return Option.Some<SaveEntry>(value);
        }

        public DBRepositoryResult TryAddOrUpdate(SaveEntry value)
        {
            var result = AddIfNotExists(value);
            if (result.HasValue)
                return DBRepositoryResult.Created | DBRepositoryResult.Success;

            if (!TryUpdate(value))
                return DBRepositoryResult.Error;

            return DBRepositoryResult.Updated | DBRepositoryResult.Success;
        }

        public bool TryUpdate(SaveEntry value)
        {
            return DapperManager.Instance.Saves.Update(value);
        }

        public DBRepositoryResult TryRemove(SaveEntry value)
        {
            // Remove creatures
            value = DapperManager.Instance.Saves.FindById(value.Id);
            foreach (var c in value.Creatures)
            {
                if (!DapperManager.Instance.Creatures.Delete(c.Id))
                    return DBRepositoryResult.Error;
            }

            // Remove the save itself
            if (!DapperManager.Instance.Saves.Delete(value.Id))
                return DBRepositoryResult.Error;

            return DBRepositoryResult.Deleted;
        }

        // == Retrieve ==

        public IEnumerable<SaveEntry> GetAll()
        {
            return DapperManager.Instance.Saves.FindAll();
        }
        public Option<SaveEntry> GetById(long id)
        {
            SaveEntry value = DapperManager.Instance.Saves
                .Include(x => x.Creatures).FindById(id);

            if (value == null)
                return Option.None<SaveEntry>();

            return Option.Some<SaveEntry>(value);
        }
        public Option<SaveEntry> GetByChecksum(uint checksum)
        {
            SaveEntry value = (DapperManager.Instance.Saves
                .Include(x => x.Creatures)
                .Find(x => x.Checksum == checksum)).FirstOrDefault();

            if (value == default)
                return Option.None<SaveEntry>();

            return Option.Some<SaveEntry>(value);
        }
    }
}
