﻿using Optional;
using RWDB.LiteDB.Enums;
using RWDB.LiteDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWDB.LiteDB.Repositories
{
    public interface ICreatureRepository<T>
    {
        Option<T> Add(SaveEntry save, T value);
        Option<T> AddIfNotExists(SaveEntry save, T value);
        DBRepositoryResult TryAddOrUpdate(SaveEntry save, T value);
        DBRepositoryResult TryUpdate(T value);
        DBRepositoryResult TryRemove(SaveEntry save, T value);
        IEnumerable<T> GetAll();
        Option<T> GetById(long id);
        Option<T> GetByGameId(SaveEntry save, int gameId);
    }
}
