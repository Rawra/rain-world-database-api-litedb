﻿using RWDB.LiteDB.Models;
using RWDB.LiteDB.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optional;
using RWDB.LiteDB.Enums;
using System.Diagnostics;
using UnityEngine;
namespace RWDB.LiteDB.Repositories
{
    /// <summary>
    /// Repository
    /// Implements CRUD Operations
    /// </summary>
    public partial class CreatureRepositoryLiteDB : ICreatureRepository<CreatureEntry>
    {
        // == Add/Update/Remove ==

        public Option<CreatureEntry> Add(SaveEntry save, CreatureEntry value)
        {
            // Add creature
            if ((DapperManager.Instance.Creatures.Insert(value)).IsNull)
                return Option.None<CreatureEntry>();

            // Add reference to save
            save.Creatures.Add(value);
            DapperManager.Instance.Saves.Update(save);

            return Option.Some<CreatureEntry>(value);
        }

        public Option<CreatureEntry> AddIfNotExists(SaveEntry save, CreatureEntry value)
        {
            if (save.Creatures.Exists(x => x.GameId == value.GameId))
                return Option.None<CreatureEntry>();

            return Add(save, value);
        }

        public DBRepositoryResult TryAddOrUpdate(SaveEntry save, CreatureEntry value)
        {
            var result = AddIfNotExists(save, value);
            if (result.HasValue)
                return DBRepositoryResult.Created | DBRepositoryResult.Success;

            if (TryUpdate(value) != DBRepositoryResult.Success)
                return DBRepositoryResult.Error;

            return DBRepositoryResult.Updated | DBRepositoryResult.Success;
        }

        public DBRepositoryResult TryUpdate(CreatureEntry value)
        {
            if (DapperManager.Instance.Creatures.Update(value))
                return DBRepositoryResult.Updated | DBRepositoryResult.Success;

            return DBRepositoryResult.Error;
        }

        public DBRepositoryResult TryRemove(SaveEntry save, CreatureEntry value)
        { 
            // Delete creature 
            if (!DapperManager.Instance.Creatures.Delete(value.Id))
                return DBRepositoryResult.Error;

            // Delete references
            save.Creatures.Remove(value);

            // Update the save so the creatures reference list gets updated aswell
            if (!DapperManager.Instance.Saves.Update(save))
                return DBRepositoryResult.Error;

            return DBRepositoryResult.Deleted | DBRepositoryResult.Success;
        }

        // == Retrieve ==

        public IEnumerable<CreatureEntry> GetAll()
        {
            return DapperManager.Instance.Creatures.FindAll();
        }
        public Option<CreatureEntry> GetById(long id)
        {
            CreatureEntry value = DapperManager.Instance.Creatures.FindById(id);
            if (value == null)
                return Option.None<CreatureEntry>();

            return Option.Some<CreatureEntry>(value);
        }
        public Option<CreatureEntry> GetByGameId(SaveEntry save, int gameId)
        {
            CreatureEntry value = save.Creatures.Find(x => x.GameId == gameId);
            if (value == null)
                return Option.None<CreatureEntry>();

            return Option.Some<CreatureEntry>(value);
        }

    }
}
