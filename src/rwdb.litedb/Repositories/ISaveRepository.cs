﻿using Optional;
using RWDB.LiteDB.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWDB.LiteDB.Repositories
{
    /// <summary>
    /// Interface: Generic database I/O
    /// Defines CRUD Methods
    /// </summary>
    public interface ISavesRepository<T>
    {
        long Add(T value);
        Option<T> AddIfNotExists(T value);
        DBRepositoryResult TryAddOrUpdate(T value);
        bool TryUpdate(T value);
        DBRepositoryResult TryRemove(T value);
        IEnumerable<T> GetAll();
        Option<T> GetById(long id);
        Option<T> GetByChecksum(uint checksum);
    }
}
