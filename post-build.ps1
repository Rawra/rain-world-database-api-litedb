param (
    [string]$sourceFolder,
    [string]$destinationFolder,
    [string]$binaryFileName,
    [string]$rootNameSpace,
    [string]$solutionDir
)

# Table array containing filenames to copy over
$fileNamesToCopy = @(
    $binaryFileName,
    "$binaryFileName.SHA256",
    "$binaryFileName.config",
	"LiteDB.dll",
	"Optional.dll",
	"Zlib.Portable.dll"
)

$destinationFolder = Join-Path -Path $destinationFolder -ChildPath $rootNameSpace.ToLower()
$destinationFolder = Join-Path -Path $destinationFolder -ChildPath "plugins"

# Check if source folder exists
if (-not (Test-Path -Path $sourceFolder -PathType Container)) {
    Write-Host "Source folder does not exist."
    exit 1
}

# Check if destination folder exists, create if it doesn't
if (-not (Test-Path -Path $destinationFolder -PathType Container)) {
    New-Item -Path $destinationFolder -ItemType Directory | Out-Null
}

# Iterate over filenames in the table array
foreach ($fileName in $fileNamesToCopy) {
    $sourceFile = Join-Path -Path $sourceFolder -ChildPath $fileName
    $destinationFile = Join-Path -Path $destinationFolder -ChildPath $fileName

    # Check if source file exists
    if (Test-Path -Path $sourceFile -PathType Leaf) {
        # Copy file to destination folder
        Copy-Item -Path $sourceFile -Destination $destinationFile -Force
        Write-Host "Copied $fileName to $destinationFolder"

        if (($fileName) -eq ($binaryFileName)) {
            # Calculate SHA256 hash for main assembly
            $sha256Hash = Get-FileHash -Path $sourceFile -Algorithm SHA256
            $destinationFile = $sourceFile + ".SHA256"

            Write-Host "Saving SHA256 checksum for $sourceFile to $destinationFile"
            $sha256Hash.Hash | Out-File -FilePath $destinationFile -Encoding ASCII
        }

    } else {
        Write-Host "File $fileName does not exist in source folder."
    }
}


# Check if gameinstalldir.txt exists
$gameInstallDirFile = Join-Path -Path $solutionDir -ChildPath "/gameinstalldir.txt"
Write-Host "File $gameInstallDirFile test"
if (Test-Path -Path $gameInstallDirFile -PathType Leaf) {
    # Read game install directory from file
    $gameInstallDir = Get-Content -Path $gameInstallDirFile
    $rootNameSpace = $rootNameSpace.ToLower();
    $gameInstallDir += "/RainWorld_Data/StreamingAssets/mods/$rootNameSpace/plugins/";

    # Check if destination folder exists
    if (Test-Path -Path $destinationFolder -PathType Container) {
        # Copy all files from destination folder to game install directory
        Copy-Item -Path $destinationFolder\* -Destination $gameInstallDir -Force
        Write-Host "Copied all files from $destinationFolder to $gameInstallDir"
    } else {
        Write-Host "Destination folder does not exist."
    }
} else {
    Write-Host "gameinstalldir.txt file does not exist. Skipping copying files to game install directory."
}



Write-Host "Files copied successfully."
